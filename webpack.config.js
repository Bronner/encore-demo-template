const Encore = require('@symfony/webpack-encore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const glob = require('glob');
const path = require('path');

const templatesPath = path.resolve('./src/templates/');
glob.sync('**/*.+(html|ejs|pug|hbs)', { cwd: templatesPath })
  .forEach((filename) => {
    Encore.addPlugin(new HtmlWebpackPlugin({
      minify: false,
      template: `${templatesPath}/${filename}`,
      filename,
    }));
  });

Encore
  .setOutputPath('static/')
  .setPublicPath('/')
  .addEntry('app', './src/js/app.js')
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableSassLoader()
  .enableEslintLoader()
  .enablePostCssLoader()
  .enableBuildCache({
    config: [__filename],
  })
  .autoProvidejQuery()
  .configureImageRule({
    type: 'asset',
    maxSize: 1024 * 8,
  })
  .configureFontRule({
    type: 'asset',
    maxSize: 1024 * 4,
  })
  .configureFilenames({
    assets: 'img/export/[name].[hash:8].[ext]',
  })
  .copyFiles({
    from: './src/img',
    to: 'img/[path][name].[ext]',
    pattern: /\.(png|jpe?g|gif|svg|webp)$/,
  })
  .copyFiles({
    from: './src/favicon',
    to: 'favicon/[path][name].[ext]',
  })
  .configureFriendlyErrorsPlugin((options) => {
    options.clearConsole = true;
  })
  .configureTerserPlugin((options) => {
    options.terserOptions = {
      format: {
        comments: false,
      },
    };
    options.extractComments = false;
  })
  .addPlugin(new ImageMinimizerPlugin({
    minimizerOptions: {
      plugins: [
        ['gifsicle', { interlaced: true }],
        ['mozjpeg', { progressive: true, quality: 85 }],
        ['pngquant', { quality: [0.6, 0.83], speed: 1 }],
        ['svgo', {
          plugins: [
            {
              name: 'removeViewBox',
              active: false,
            },
            {
              name: 'removeUnknownsAndDefaults',
              params: { unknownAttrs: false },
            },
            {
              name: 'cleanupIDs',
              active: false,
            },
            {
              name: 'collapseGroups',
              active: false,
            },
          ],
        },
        ],
      ],
    },
  }));

module.exports = Encore.getWebpackConfig();
